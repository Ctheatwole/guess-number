import React from 'react';
import { Text, StyleSheet } from 'react-native';
import Colors from '../constants/colors';

const TitleText = props => <Text style={{...styles.body, ...props.style}}>{props.children}</Text>

const styles = StyleSheet.create({
    body: {
        fontSize: 18,
        color: Colors.primaryDarkFont,
        fontFamily: 'open-sans-bold',
    }
});

export default TitleText;