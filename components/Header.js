import React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import Colors from '../constants/colors';
import BodyText from './BodyText';
import TitleText from './TitleText';

const Header = props => {
    return (
        <View style={styles.header}>
            <TitleText style={{...styles.headerTitle, ...props.style}}>{props.title}</TitleText>
        </View>
    )
};

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 90,
        paddingTop: 36,
        backgroundColor: Colors.quaternary,
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerTitle: {
        color: Colors.primaryLightFont
    }
});

export default Header;

