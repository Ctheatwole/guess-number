import React, { useState } from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button,
    TouchableWithoutFeedback,
    Keyboard,
    Alert
    } from 'react-native';
import { EvilIcons } from '@expo/vector-icons';

import Card from '../components/Card';
import Colors from '../constants/colors';
import Configs from '../constants/configs';
import Input from '../components/Input';
import NumberContainer from '../components/NumberContainer';
import BodyText from '../components/BodyText';
import TitleText from '../components/TitleText';
import PrimaryButton from '../components/PrimaryButton';
import SecondaryButton from '../components/SecondaryButton';
import configs from '../constants/configs';

const StartGameScreen = props => {
    const [enteredValue, setEnteredValue] = useState('');
    const [confirmed, setConfirmed] = useState(false);
    const [selectedNumber, setSelectedNumber] = useState();

    const numberInputHandler = inputText => {
        setEnteredValue(inputText.replace(/[^0-9]/g, '')); // replace any non-numeric text with an empty string
    };
    const resetButtonHandler = () => {
        Keyboard.dismiss();
        setEnteredValue('');
        setConfirmed(false);
        setSelectedNumber();
    }
    const confirmButtonHandler = () => {
        Keyboard.dismiss();
        const chosenNumber = parseInt(enteredValue);

        // if the value is not a number, just don't do anything
        if (isNaN(chosenNumber)) {
            return;
        }

        // if the value is a number that is not in appropriate range, let the user know
        if (chosenNumber <= 0 || chosenNumber > 99) {
            Alert.alert("Invalid entry", "Value should be a number between 1 and 99",
                [{text: 'Got it!', style: 'cancel', onPress: resetButtonHandler}]
            )
            return;
        }
        setConfirmed(true);
        // entered value will change after React batches these commands,
        // which means there is no risk of the next line not having the
        // correct value for enteredValue. in other words, order doesn't
        // matter because React will execute in the same render cycle
        setEnteredValue('');
        setSelectedNumber(chosenNumber);
    }

    let confirmedOutput;
    if (confirmed) {
        confirmedOutput = (
            <Card style={styles.summaryContainer}>
                <BodyText>You selected</BodyText>
                <NumberContainer>{selectedNumber}</NumberContainer>
                <PrimaryButton onPress={() => props.onStartGame(selectedNumber)}>Start game!</PrimaryButton>
            </Card>
        );
    }

    return (
        <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }} >
            <View style={styles.screen}>
            <TitleText style={styles.title}>Start a new game!</TitleText>
            <Card style={styles.inputContainer}>
                <BodyText>Select a number</BodyText>
                <Input 
                    style={ styles.input } 
                    onChangeText={numberInputHandler}
                    blurOnSubmit
                    autoCorrect={false}
                    keyboardType='phone-pad'
                    maxLength={2}
                    autoCapitalize='none'
                    value={ enteredValue }
                />
                <View style={ styles.buttonContainer }>
                    <SecondaryButton onPress={resetButtonHandler} style={{color: Colors.secondary}}>
                        <EvilIcons name={'close-o'} size={configs.iconSize}></EvilIcons>
                    </SecondaryButton>
                    <PrimaryButton onPress={confirmButtonHandler} style={{color: Colors.primary}}>
                        <EvilIcons name={'check'} size={configs.iconSize}></EvilIcons>
                    </PrimaryButton>
                </View>
            </Card>
            {confirmedOutput}
        </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10,
        alignItems: 'center'
    },
    title: {
        marginVertical: 10,
        color: Colors.primaryDarkFont
    },
    inputContainer: {
        width: 300,
        maxWidth: '80%',
        alignItems: 'center'
    },
    buttonContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-evenly',
        paddingHorizontal: 15
    },
    button: {
        flex: 1,
        paddingHorizontal: 10
    },
    input: {
        width: 50,
        textAlign: 'center'
    },
    chosenNumber: {
        margin: 8,
        fontSize: 20
    },
    summaryContainer: {
        marginTop: 20,
        alignItems: 'center'
    }
});

export default StartGameScreen;

