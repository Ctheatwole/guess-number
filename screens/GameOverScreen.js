import React from 'react';
import { View, Text, StyleSheet, Button, Image } from 'react-native';
import Colors from '../constants/colors';
import BodyText from '../components/BodyText';
import TitleText from '../components/TitleText';
import PrimaryButton from '../components/PrimaryButton';

const GameOverScreen = props => {
  return (
      <View style={styles.screen}>
        <TitleText style={styles.gameOverTitle}>The game is over</TitleText>
        {/* resizeMode="contain" maintains aspect ratio, but fits entire image into whatever width/height you give it */}
        <View style={styles.imageContainer} resizeMode="contain" >
          <Image
            fadeDuration={10000}
            loadingIndicatorSource={require('../assets/img/splash.png')}
            source={require('../assets/img/success.png')} style={styles.image} 
            // source={{uri: 'https://facebook.github.io/react-native/img/tiny_logo.png'}}
            style={styles.image}
            resizeMode="cover"
          />
        </View>
        <View style={styles.resultContainer}>
          <BodyText style={styles.resultText}>
            Your phone needed{' '}<Text style={styles.highlight}>{props.rounds}</Text>{' '}rounds
            to guess the number{' '}<Text style={styles.highlight}>{props.guessNumber}</Text>
          </BodyText>
        </View>
        <PrimaryButton onPress={props.onStartNewGame} >Start new game</PrimaryButton>
      </View>
    );
};

const styles = StyleSheet.create({
    screen: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    gameOverTitle: {
      fontSize: 24,
      marginVertical: 20
    },
    image: {
      width: '100%',
      height: '100%'
    },
    imageContainer: {
      borderRadius: 150,
      borderWidth: 3,
      borderColor: 'black',
      width: 300,
      height: 300,
      overflow: 'hidden',
      marginVertical: 30
    },
    resultContainer: {
      marginHorizontal: 40,
      marginVertical: 15
    },
    resultText: {
      textAlign: 'center',
      fontSize: 20
    },
    highlight:
    {
      color: Colors.primary,
      fontFamily: 'open-sans-bold'
    }
});

export default GameOverScreen;
