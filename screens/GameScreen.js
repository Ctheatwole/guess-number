import React, { useState, useRef, useEffect } from 'react';
import { View, Text, StyleSheet, Button, Alert, ScrollView, FlatList } from 'react-native';
import { Ionicons, EvilIcons } from '@expo/vector-icons';

import NumberContainer from '../components/NumberContainer';
import Card from '../components/Card';
import Colors from '../constants/colors';
import Configs from '../constants/configs';
import TitleText from '../components/TitleText';
import SecondaryButton from '../components/SecondaryButton';
import PrimaryButton from '../components/PrimaryButton';
import BodyText from '../components/BodyText';

const generateRandomBetween = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  const rndNum = Math.floor(Math.random() * (max - min)) + min;
  return rndNum;
};

const renderListItem = (listLength, itemData) => {
    return (
        <View key={ itemData.index } style={styles.listItem} >
            <BodyText>#{ listLength - itemData.index }</BodyText>
            <BodyText>{ itemData.item }</BodyText>
        </View>
    );
}

const GameScreen = props => {
  
  const floor = useRef(1);
  const ceiling = useRef(100);
  const initialGuess = generateRandomBetween(floor.current, ceiling.current);
  
  const [currentGuess, setCurrentGuess] = useState(initialGuess);
  const [pastGuesses, setPastGuesses] = useState([initialGuess.toString()]);

  const { userChoice, onGameOver} = props;

  useEffect(() => {
      if (currentGuess === userChoice) {
        props.onGameOver(pastGuesses.length);
      }
  }, [currentGuess, userChoice, onGameOver]);

  const nextGuessHandler = direction => {
    if (direction === 'L' && currentGuess < props.userChoice) {
        Alert.alert('Hmmmm...', 'Looks like the number you chose is greater...',
            [{text: 'Fine', style: 'cancel'}]
        );
        return;
    }
    else if (direction === 'G' && currentGuess > props.userChoice) {
        Alert.alert('Hmmmm...', 'Looks like the number you chose is lower...',
            [{text: 'Fine', style: 'cancel'}]
        );
        return;
    }

    if (direction === 'L'){
        // guess a lower number
        ceiling.current = currentGuess;
    }
    else /*if (direction === 'G')*/ {
        // guess a higher number
        floor.current = currentGuess + 1;
    }

    var nextNumber = generateRandomBetween(floor.current, ceiling.current);
    setPastGuesses(curPastGuess => [nextNumber.toString(), ...curPastGuess]);
    setCurrentGuess(nextNumber);
  };

  return (
    <View style={styles.screen}>
      <TitleText>Opponent's Guess</TitleText>
      <NumberContainer>{currentGuess}</NumberContainer>
      <Card style={styles.card}>
        <View style={ styles.buttonContainer }>
          <SecondaryButton onPress={nextGuessHandler.bind(this, 'L')}>
            <EvilIcons name={'minus'} size={Configs.iconSize} />
          </SecondaryButton>
          <PrimaryButton onPress={nextGuessHandler.bind(this, 'G')}>
            <EvilIcons name={'plus'} size={Configs.iconSize} />
          </PrimaryButton>
        </View>
      </Card>
      <View style={styles.listContainer}>
        {/* <ScrollView contentContainerStyle={styles.list} >
            {pastGuesses.map((guess, index) => renderListItem(guess, pastGuesses.length - index))}
        </ScrollView>  */}
        <FlatList 
            keyExtractor={(item) => item}
            data={pastGuesses}
            renderItem={renderListItem.bind(this, pastGuesses.length)}
        />
      </View>
      
    </View>
  );

};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    alignItems: 'center'
  },
  // buttonContainer: {
  //   flexDirection: 'row',
  //   justifyContent: 'space-around',
  //   marginTop: 20,
  //   width: 400,
  //   maxWidth: '80%'
  // },
  card: {
    width: 300,
    maxWidth: '80%',
    alignItems: 'center'
  },
  buttonContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-evenly',
    paddingHorizontal: 15
  },
  listContainer: {
    flex: 1,
    width: '60%',
  },
  list: {
    flexGrow: 1, // valuable for scrollviews, behaves similarly to flex
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  listItem: {
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    padding: 15,
    marginVertical: 10,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%'
  }
});

export default GameScreen;
