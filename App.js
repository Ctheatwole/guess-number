import React, {useState} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';

import Header from './components/Header';
import StartGameScreen from './screens/StartGameScreen';
import GameScreen from './screens/GameScreen';
import GameOverScreen from './screens/GameOverScreen';

const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
  });
};

export default function App() {

  const [userNumber, setUserNumber] = useState();
  const [rounds, setRounds] = useState(0);
  const [ready, setReady] = useState(false);

  // This logic will behave similarly to $(document).ready(), use to load
  // fonts and images when you don't want the app to try to run without them
  if (!ready) {
    return (
      <AppLoading
          startAsync={fetchFonts}
          onFinish={() => setReady(true)}
          onError={(err) => console.log(err)}
         />
    )
  }

  const startGameHandler = (selectedNumber) => {
    setUserNumber(selectedNumber);
    setRounds(0);
  };

  const gameOverHandler = (rounds) => {
    setRounds(rounds);
  };

  const newGameHandler = () => {
      setUserNumber(null);
      setRounds(0);
  };

  let content = <StartGameScreen onStartGame={startGameHandler} />;

  if (userNumber && rounds <= 0) {
    content = <GameScreen userChoice={userNumber} onGameOver={gameOverHandler} />;
  }
  else if (rounds > 0) {
    content = <GameOverScreen rounds={rounds} guessNumber={userNumber} onStartNewGame={newGameHandler} />;
  }

  return (
    <View style={styles.screen}>
      <Header style={styles.header} title={"Guess a number"} />
      {content}
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1 // takes full width and height of screen
  }
});
