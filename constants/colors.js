export default {
    primary: '#011BFF',
    secondary: '#851CFF',
    tertiary: '#3D17EB',
    quaternary: '#1565EB',
    quinary: '#14ACFF',
    primaryDarkFont: '#1F1F1F',
    primaryLightFont: '#F0F0F0'
}